const path = require('path');
const Dotenv = require('dotenv-webpack');
module.exports = env => {
    return {
        entry: './src/front/app.ts',
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: 'ts-loader',
                    exclude: /node_modules/
                }
            ]
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js']
        },
        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, 'public/dist')
        },
        mode: 'development',
        performance: {hints: false},
        watch: false,
        plugins: [
            new Dotenv()
        ],
    }
};
