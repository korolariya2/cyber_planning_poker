import {RoomManager} from './entity/RoomManager';
import {Player} from './entity/Player';

const app    = require('express')();
const server = require('http').Server(app);
const io     = require('socket.io')(server);

export class SocketBundle {
    private players: Player[] = [];

    constructor() {
        server.listen(8085);

        const roomManager = new RoomManager(io.socket);

        io.on('connection', (socket: any) => {
            this.players.push(new Player(socket, roomManager));
        });
        setInterval(() => {
            const playerIndex = this.players.findIndex(player => !player.socket);
            const roomIndex   = roomManager.rooms.findIndex(room => room.players.length === 0);
            if (playerIndex >= 0) {
                delete this.players[playerIndex];
                this.players.splice(playerIndex, 1);
            }
            if (roomIndex >= 0) {
                delete roomManager.rooms[roomIndex];
                roomManager.rooms.splice(roomIndex, 1);
            }

        }, 1000);
    }
}
