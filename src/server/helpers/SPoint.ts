import {IPoint} from '../../front/interfaces/IPoint';

export class SPoint implements IPoint {
    public x: number;
    public y: number;

    constructor(x: number = 0, y: number = 0) {
        this.x = x;
        this.y = y;
    }

    public add(point: IPoint) {
        this.x += point.x;
        this.y += point.y;
    }

    public isNull() {
        return !(this.x || this.y);
    }
}
