import * as path from 'path';
import {SocketBundle} from './socket';

const express = require('express');
const app     = express();

app.use(express.static('public'));

app.get('/', (req: any, res: any) => {
    res.sendFile(path.join(__dirname + '../public/index.html'));
});
app.listen(3000, () => {
});

const sb = new SocketBundle();
