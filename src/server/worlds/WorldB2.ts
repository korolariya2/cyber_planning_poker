import b2Body = Box2D.Dynamics.b2Body;

const box2d = require('box2dweb/box2d.js');

export class WorldB2 {
    public id: number;
    public gravity: Box2D.Common.Math.b2Vec2 = new box2d.Common.Math.b2Vec2(0, 0);
    public world: Box2D.Dynamics.b2World     = new box2d.Dynamics.b2World(this.gravity, true);
    public timeStep: number                  = 1 / 60;
    public velocityIterations: number        = 10;
    public positionIterations: number        = 10;
    public hz                                = 60;

    public size = 20;

    public stop = false;

    constructor() {
        this.update();
        console.log('world created');
    }

    public update() {
        setInterval(() => {
            if (!this.stop) {
                this.world.Step(this.timeStep, this.velocityIterations, this.positionIterations);
                this.world.ClearForces();
                // console.log(that.body.GetPosition());
                // console.log(this.world.Validate());
            }
        }, 1000 / 60);
    }

}
