import {Box2DEntity} from './abstract/Box2DEntity';
import {WorldB2} from '../worlds/WorldB2';
import {Tools} from '../../front/helpers/Tools';

const box2d = require('box2dweb/box2d.js');

export class Leaf extends Box2DEntity {
    public radius = 5;
    public id: number;

    constructor(id: number, world: WorldB2) {
        super();
        this.id = id;
        this.setWorld(world);
        const b2Body = box2d.Dynamics.b2Body;
        this.createBody(b2Body.b2_dynamicBody, 'circle');
        this.setPosition(100, 100);

        setInterval(() => {

            const v = (this.getRandom(1, 2) > 1) ? 1 : -1;

            this.body.ApplyImpulse(new box2d.Common.Math.b2Vec2(v * this.getRandom(1, 3), v * this.getRandom(1, 3)), this.body.GetWorldCenter());
        }, this.getRandom(1000, 5000));
        // this.body.SetLinearVelocity(new box2d.Common.Math.b2Vec2(20, 15));
    }

    private getRandom(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
