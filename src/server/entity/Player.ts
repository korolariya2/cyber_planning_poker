import {ENTITY_TYPES, Room} from './Room';
import {RoomManager} from './RoomManager';
import {Box2DEntity} from './abstract/Box2DEntity';
import {Keys} from '../../front/helpers/Keys';
import {SPoint} from '../helpers/SPoint';
import {Leaf} from './Leaf';
import {SnowBall} from './SnowBall';

const box2d = require('box2dweb/box2d.js');

export class Player extends Box2DEntity {

    public radius: 50;
    public name: string;
    public room: Room;
    public socket: any;
    public roomManager: RoomManager;
    public position: Box2D.Common.Math.b2Vec2 = new box2d.Common.Math.b2Vec2(0, 0);

    public keys = new Keys();

    public speed = 1.1;

    public vectorD: any = {x: 0, y: 0};

    constructor(socket: any, roomManager: RoomManager) {
        super();
        this.name        = 'unNamed';
        this.room        = null;
        this.socket      = socket;
        this.roomManager = roomManager;
        this.listeners();
        setInterval(() => {
            this.update();
        }, 100);
    }

    public initBody() {
        const b2Body = box2d.Dynamics.b2Body;
        this.createBody(b2Body.b2_dynamicBody, 'circle');
        this.body.ApplyImpulse(new box2d.Common.Math.b2Vec2(1, 1), this.body.GetWorldCenter());
        // this.body.SetLinearVelocity(new box2d.Common.Math.b2Vec2(10, 10));
        this.room.notifyPlacement({
            id      : this.socket.id,
            position: this.getPosition(),
            time    : Date.now(),
        });
    }

    public update() {
        this.moveByKeyboard();
    }

    public moveByKeyboard() {
        const direction = new SPoint();
        Object.keys(this.keys.keys)
            .filter(key => this.keys.keys[key] === true)
            .map((key) => {
                if (!this.keys.direction[key]) {
                    return;
                }
                if (key === 'shift') {
                    this.speed = 15;
                }
                direction.add(this.keys.direction[key]);
            });
        Object.keys(this.keys.keys)
            .filter(key => this.keys.keys[key] === false)
            .map((key) => {
                if (key === 'shift') {
                    this.speed = 10;
                }
            });

        if (!this.body || !this.room) {
            return;
        }

        if (this.body.GetLinearVelocity().Length() > this.speed) {
            return;
        }

        const v = new box2d.Common.Math.b2Vec2(direction.x, direction.y);

        if (direction.isNull()) {
            return;
        }

        const normal = new box2d.Common.Math.b2Vec2();
        normal.x     = v.x / v.Length();
        normal.y     = v.y / v.Length();
        normal.Multiply(this.speed);

        // this.body.SetLinearVelocity(normal);

        this.body.ApplyImpulse(normal, this.body.GetWorldCenter());
        // console.log(normal);
        // console.log(this.body.GetLinearVelocity().Length());
    }

    public listeners() {
        /** @param {{playerName}} data */
        this.socket.on('enterName', (data: any) => {
            this.name = data.playerName;
            if (this.room) {
                this.room.notifyUpdatePlayerName(this.socket.id, this.name);
            }
        });

        this.socket.on('createRoom', (data: any) => {
            if (!this.room) {
                this.room = this.roomManager.createRoom(data.roomName);
                if (!this.room.players.length) {
                    this.room.owner = this;
                }
                this.room.addPlayer(this);
            }
            const dataToClient = {
                roomName : this.room.name,
                roomIndex: this.room.id,
                owner    : true,
            };
            // answer to owner Room
            this.socket.emit('onCreatedRoom', dataToClient);
            this.setWorld(this.room.world);
            this.initBody();
        });

        this.socket.on('vote', (data: number) => {
            if (!this.room) {
                this.socket.emit('onAlert', 'Please join a room.');
                return;
            }

            const voted = this.room.votes.find((vote) => {
                return vote.player === this;
            });

            if (!voted) {
                this.room.votes.push({sp: data, player: this});
            } else {
                this.socket.emit('onAlert', 'You already voted.');
            }

            this.room.notifyPlayerVoted();
        });

        this.socket.on('joinRoom', (name: string) => {
            this.roomManager.joinRoomByName(name, this);
            this.setWorld(this.room.world);
            this.initBody();
        });

        this.socket.on('showVotes', () => {
            if (!this.room) {
                return;
            }
            if (this.room.owner === this) {
                this.room.notifyShowVotes();
            }
        });

        this.socket.on('disconnect', () => {
            this.socket.emit('onAlert', 'Player has been disconnected');

            if (this.room) {
                this.room.leaveRoom(this);
            }
            this.room   = null;
            this.socket = null;
        });

        this.socket.on('onReset', () => {
            if (this.room && this.room.owner === this) {
                this.room.reset();
            } else {
                this.socket.emit('onAlert', 'You doesn\'t owner this room');
            }
        });

        this.socket.on('onNewPlacement', (point: any) => {
            if (!this.room) {
                return;
            }

            const pos      = this.getPosition();
            // const v   = {x: 0, y: 0};
            this.vectorD.x = point.x - pos.x;
            this.vectorD.y = point.y - pos.y;

            this.body.SetAngle(Math.atan2(this.vectorD.y, this.vectorD.x));

            // this.createSnowBall(new box2d.Common.Math.b2Vec2(v.x, v.y));

            // const len = Math.sqrt(Math.pow(v.x, 2) + Math.pow(v.y, 2));
            // const i   = {x: 0, y: 0};
            //
            // i.x = v.x / len;
            // i.y = v.y / len;

            // const speed = 20;

            // this.body.ApplyImpulse(new box2d.Common.Math.b2Vec2(speed * i.x, speed * i.y), this.body.GetWorldCenter());
        });

        this.socket.on('changeOwner', (id: string) => {
            this.room.changeOwner(this, id);
        });

        this.socket.on('keyPressed', (bitMask: number) => {
            this.keys.deconstructInputBitmask(bitMask);

            if (this.keys.keys.space) {
                this.body.SetLinearVelocity(new box2d.Common.Math.b2Vec2(.1, .1));
                const ball = this.createSnowBall();
                this.room.notifyPlayerShot({
                    id        : ball.id,
                    socket_id : this.socket.id,
                    position  : ball.getPosition(),
                    entityType: ENTITY_TYPES.SnowBall,
                });
            }
        });
    }

    public createSnowBall() {
        const direction = new box2d.Common.Math.b2Vec2(this.vectorD.x, this.vectorD.y);
        const x         = direction.x / direction.Length();
        const y         = direction.y / direction.Length();
        const normal    = new box2d.Common.Math.b2Vec2(x, y);

        const v = normal.Copy();
        v.Multiply(50);
        const pos = this.getPosition().Copy();
        pos.Add(v);

        const snowBall = new SnowBall(this.room, this.room.world, pos);
        this.room.entities.push(snowBall);

        normal.Multiply(20);
        snowBall.pushToVector(normal);

        return snowBall;
    }

    public damage() {
        this.room.notifyOnDamage({
            player_id: this.socket.id,
        });
    }
}
