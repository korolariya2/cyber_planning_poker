import {Player} from './Player';
import {WorldB2} from '../worlds/WorldB2';
import {Leaf} from './Leaf';
import {SnowBall} from './SnowBall';
import {Box2DEntity} from './abstract/Box2DEntity';

export const ENTITY_TYPES = {
    Player  : 1,
    Leaf    : 2,
    SnowBall: 2,
};

export class Room {
    public id: number;
    public name: string;
    public players: Player[] = [];
    public limit: number;
    public owner: Player;

    public votes: Array<{ sp: number, player: Player }> = [];

    public world = new WorldB2();

    public entities: Box2DEntity[] = [];

    public counterEntity = 0;

    public map: any = [];

    constructor(id: number, name: string) {
        this.id      = id;
        this.name    = name;
        this.players = [];
        this.limit   = 100;
        // this.createLeafs();
        this.createMatrix();

        setInterval(() => {
            this.players.forEach((player) => {
                this.notifyPlacement({
                    id        : player.socket.id,
                    position  : player.getPosition(),
                    time      : Date.now(),
                    entityType: ENTITY_TYPES.Player,
                    angle     : player.body.GetAngle(),
                });
            });
        }, 100);

        setInterval(() => {
            this.entities.forEach((entity) => {
                this.notifyPlacement({
                    id        : entity.id,
                    position  : entity.getPosition(),
                    time      : Date.now(),
                    entityType: ENTITY_TYPES.Leaf,
                    angle     : entity.body.GetAngle(),
                });
            });
        }, 100);

        setInterval(() => {
            this.notifyMap();
        }, 1000);
    }

    public createMatrix() {
        for (let i = 0; i < 1000; i++) {
            this.map[i] = [];
            for (let j = 0; j < 1000; j++) {
                this.map[i][j] = null;
            }
        }
    }

    public findByIdMatrix(id: any) {
        for (let i = 0; i < 1000; i++) {
            for (let j = 0; j < 1000; j++) {
                if (this.map[i][j] && this.map[i][j].id === id) {
                    return [i, j];
                }
            }
        }
        return [null, null];
    }

    public findMatrix() {
        const response = [];
        for (let i = 0; i < 1000; i++) {
            for (let j = 0; j < 1000; j++) {
                if (this.map[i][j]) {
                    response.push({obj: this.map[i][j], x: i, y: j});
                }
            }
        }
        return response;
    }

    public setPlayerToMatrix(point: { x: number, y: number }, id: any) {
        const [i, j] = this.findByIdMatrix(id);
        if (i && j) {
            this.map[i][j] = null;
        }
        const [x, y]   = [Math.ceil(point.x / 50 + 500), Math.ceil(point.y / 50 + 500)];
        this.map[x][y] = {type: ENTITY_TYPES.Player, id: id};
    }

    public addPlayer(player: Player) {
        if (this.players.find(p => p === player)) {
            return true;
        }

        if (this.players.length < this.limit) {
            this.players.push(player);
            player.room = this;
            this.notifyJoinedPlayer();
            player.socket.emit('onJoinRoom');
            return true;
        }
        return false;
    }

    public getDataPlayers() {
        return this.players.map((player) => {
            if (player.socket) {
                return {
                    id      : player.socket.id,
                    name    : player.name,
                    owner   : (player === this.owner),
                    position: player.getPosition(),
                };
            }
        });
    }

    public notifyJoinedPlayer() {
        this.players.forEach((player) => {
            player.socket.emit('playerJoined', this.getDataPlayers());
        });
    }

    public notifyLeavePlayer(player: Player) {

        const data = this.players.map((itemPlayer) => {
            if (itemPlayer.socket) {
                return itemPlayer.socket.id;
            }
        });

        this.players.forEach((itemPlayer) => {
            itemPlayer.socket.emit('syncPlayers', data);
        });
    }

    public notifyPlayerVoted() {
        const data = this.votes.map((item) => {
            if (item.player.socket) {
                return item.player.socket.id;
            }
        });

        this.players.forEach((player) => {
            player.socket.emit('onVotedPlayers', data);
        });
    }

    public notifyShowVotes() {
        const data = this.votes.map((item) => {
            if (item.player.socket) {
                return {
                    id  : item.player.socket.id,
                    sp  : item.sp,
                    name: item.player.name,
                };
            }
        });

        this.players.forEach((player) => {
            player.socket.emit('onShowVotes', data);
        });
    }

    public leaveRoom(player: Player) {
        const index = this.players.findIndex(p => p === player);
        this.players.splice(index, 1);

        if (this.owner === player) {
            this.owner = this.players[0];
        }

        const voteIndex = this.votes.findIndex(item => item.player === player);
        this.votes.splice(voteIndex, 1);

        this.notifyLeavePlayer(player);
        this.notifyJoinedPlayer();
    }

    public reset() {
        this.votes = [];
        this.notifyAllReset();
    }

    public notifyAllReset() {
        this.players.forEach((player) => {
            player.socket.emit('onReset');
        });
    }

    public notifyNewPlacement(data: any) {
        this.players.forEach((player) => {
            player.socket.emit('onTarget', data);
        });
    }

    public notifyMap() {
        this.players.forEach((player) => {
            player.socket.emit('onMap', this.findMatrix());
        });
    }

    public notifyPlacement(data: any) {
        if (data.entityType === ENTITY_TYPES.Player) {
            this.setPlayerToMatrix(data.position, data.id);
        }

        this.players.forEach((player) => {
            player.socket.emit('onPosition', data);
        });
    }

    public notifyUpdatePlayerName(id: string, name: string) {
        this.players.forEach((player) => {
            player.socket.emit('enterName', {
                id  : id,
                name: name,
            });
        });
    }

    public changeOwner(player: Player, id: string) {
        if (player !== this.owner) {
            return;
        }
        const foundPlayer = this.players.find(p => p.socket.id === id);
        if (foundPlayer) {
            this.owner = foundPlayer;
            this.notifyJoinedPlayer();
        }
    }

    public notifyPlayerShot(obj: any) {
        this.players.forEach((player) => {
            player.socket.emit('playerShot', obj);
        });
    }

    public notifySnowDestroy(obj: any) {
        this.players.forEach((player) => {
            player.socket.emit('snowBallDestroy', obj);
        });
    }

    public notifyOnDamage(obj: any) {
        this.players.forEach((player) => {
            player.socket.emit('damage', obj);
        });
    }
}
