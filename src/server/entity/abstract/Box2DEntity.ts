import {WorldB2} from '../../worlds/WorldB2';

const box2d = require('box2dweb/box2d.js');

export const ENTITY_TYPES = {
    'circle': 1,
    'box'   : 2,
};

export abstract class Box2DEntity {
    public id: number;
    public body: Box2D.Dynamics.b2Body;
    public worldB2: WorldB2;
    public radius: number = 20;
    public width: number  = 1000;
    public height: number = 20;
    public type: any      = ENTITY_TYPES.box;

    public position: any = new box2d.Common.Math.b2Vec2(1, 1);

    public createBody(type: any, form: string) {
        const b2BodyDef      = box2d.Dynamics.b2BodyDef;
        const b2FixtureDef   = box2d.Dynamics.b2FixtureDef;
        const b2CircleShape  = box2d.Collision.Shapes.b2CircleShape;
        const b2PolygonShape = box2d.Collision.Shapes.b2PolygonShape;

        const bodyDef: any = new b2BodyDef();
        bodyDef.type       = type;

        bodyDef.position.Set(this.position.x / this.worldB2.size, this.position.y / this.worldB2.size);
        bodyDef.linearDamping  = 0.5;
        bodyDef.angularDamping = 0.1;

        this.body = this.worldB2.world.CreateBody(bodyDef);

        let shape: any;
        switch (form) {
            case 'circle':
                shape = new b2CircleShape(this.radius / this.worldB2.size);
                break;
            default :
                shape = new b2PolygonShape();
                shape.SetAsBox(this.width / 2 / this.worldB2.size, this.height / 2 / this.worldB2.size);
                break;
        }

        const fd: any  = new b2FixtureDef();
        fd.shape       = shape;
        fd.density     = 1;
        fd.restitution = 0.5;
        fd.friction    = 0.5;

        this.body.CreateFixture(fd);
    }

    public setWorld(worldB2: WorldB2) {
        this.worldB2 = worldB2;
    }

    public getPosition() {
        if (!this.body) {
            return this.position;
        }
        const position = this.body.GetPosition().Copy();
        position.Multiply(this.worldB2.size);
        return position;
    }

    public setPosition(x: number, y: number) {
        x *= 1 / 20;
        y *= 1 / 20;
        this.body.SetPosition(new box2d.Common.Math.b2Vec2(x, y));
    }

    public getData() {
        const tp: any = {};
        tp.entityId   = this.id;
        tp.position   = this.getPosition();
        tp.width      = this.width;
        tp.height     = this.height;
        tp.radius     = this.radius;
        tp.entityType = this.type;
        return tp;
    }

    public destroy() {
        this.worldB2.world.DestroyBody(this.body);
    }
}
