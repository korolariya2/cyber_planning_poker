import {Box2DEntity} from './abstract/Box2DEntity';
import {WorldB2} from '../worlds/WorldB2';
import {Room} from './Room';

const box2d = require('box2dweb/box2d.js');

export class SnowBall extends Box2DEntity {
    public radius = 10;
    public id: number;

    public room: Room;

    public listener = new box2d.Dynamics.b2ContactListener();

    public timeOut: any;

    constructor(room: Room, world: WorldB2, position: Box2D.Common.Math.b2Vec2) {
        super();
        this.room = room;
        this.room.counterEntity++;
        this.id = this.room.counterEntity;
        this.setWorld(world);
        const b2Body = box2d.Dynamics.b2Body;
        this.createBody(b2Body.b2_dynamicBody, 'circle');
        this.setPosition(position.x, position.y);

        this.timeOut = setTimeout(() => {
            this.room.notifySnowDestroy({
                id      : this.id,
                position: this.getPosition(),
            });
            this.destroy();
        }, 5000);

        this.listener.BeginContact = (contact: any) => {
            if (contact.GetFixtureA().GetBody() === this.body) {
                this.room.notifySnowDestroy({
                    id      : this.id,
                    position: this.getPosition(),
                });
                this.destroy();
            }
            const foundPlayer = this.room.players.find(player => player.body === contact.GetFixtureB().GetBody());
            if (!foundPlayer) {
                return;
            }
            foundPlayer.damage();
        };
        this.worldB2.world.SetContactListener(this.listener);
    }

    public pushToVector(v: Box2D.Common.Math.b2Vec2) {
        this.body.ApplyImpulse(v, this.body.GetWorldCenter());
    }

    public destroy() {
        super.destroy();
        const index = this.room.entities.findIndex(item => item.id === this.id);
        this.room.entities.splice(index, 1);
        clearTimeout(this.timeOut);
    }

    private getRandom(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

}
