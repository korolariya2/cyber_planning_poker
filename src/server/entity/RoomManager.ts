import {Room} from './Room';
import {Player} from './Player';

export class RoomManager {

    public counterRooms: number = 0;
    public rooms: Room[]        = [];

    public socket: any;

    constructor(socket: any) {
        this.socket = socket;
    }

    public createRoom(name: string) {
        this.counterRooms++;

        const foundRoom = this.rooms.find((room) => {
            return room.name === name;
        });

        if (foundRoom) {
            return foundRoom;
        }

        const newRoom = new Room(this.counterRooms, name);
        this.rooms.push(newRoom);
        return newRoom;
    }

    public joinRoomByName(name: string, player: Player): Room | boolean {
        const room = this.rooms.find((itemRoom) => {
            return itemRoom.name === name;
        });

        if (room && room.addPlayer(player)) {
            return room;
        }

        return false;
    }
}
