import {Container, Service} from 'typedi';
import {PlayerManager} from './PlayerManager';
import {ENTITY_TYPES} from '../server/entity/Room';
import {EntityManager} from './EntityManager';
import {ITransferObject, TransferObject} from './helpers/DataTransfer';
import {plainToClass} from 'class-transformer';
import {PlayersList} from './entities/PlayersList';
import {Point} from './helpers/cyber/Point';

const io = require('socket.io-client');

declare const process: any;

function Speaker(target: any, key: any, descriptor: any) {
    const originalMethod = descriptor.value;
    descriptor.value     = function () {
        const socket: any = originalMethod.apply(this, arguments);
        socket.on(key, (data: any) => {
            const net = Container.get(Network);
            if (net.subscribers.hasOwnProperty(key)) {
                net.subscribers.onMap.map((callback: any) => {
                    callback(data);
                });
            }
        });

        return originalMethod.apply(this, arguments);
    };
    return descriptor;
}

@Service()
export class Network {
    public socket: any;

    public map: any;

    public subscribers: any = {};

    constructor() {
        this.socket = io(`http://${process.env.SERVER_HOST}:8085`);
        this.listeners();
    }

    public addListener(key: string, callback: any) {
        if (!this.subscribers[key]) {
            this.subscribers[key] = [];
        }
        this.subscribers[key].push(callback);
    }

    public listeners() {
        this.onEnterName();
        // this.onCreatedRom();
        this.onJoinRoom();
        this.onVoted();
        // this.onPlayerList();
        this.onPlayerJoined();
        this.onShowVotes();
        this.onSyncPlayers();
        // this.onAlert();
        this.onReset();
        this.onPosition();
        this.onShot();
        this.onSnowDestroy();
        this.onDamage();
        this.onMap();
    }

    // ----- Listeners -----
    public onEnterName() {
        this.socket.on('enterName', (data: { id: string, name: string }) => {
            const pm = Container.get(PlayerManager);
            pm.updatePlayerName(data);
        });
    }

    public onVoted() {
        this.socket.on('onVotedPlayers', (data: any) => {
            const pm = Container.get(PlayerManager);
            pm.checkVotedPlayers(data);
        });
    }

    public onJoinRoom() {
        this.socket.on('onJoinRoom', () => {
            const pm = Container.get(PlayerManager);
            pm.createDeck();
        });
    }

    public onSyncPlayers() {
        this.socket.on('syncPlayers', (data: any) => {
            const pm = Container.get(PlayerManager);
            pm.removePlayer(data);
        });
    }

    public onPlayerJoined() {
        this.socket.on('playerJoined', (data: any) => {
            const pm = Container.get(PlayerManager);
            pm.createPlayers(data);
        });
    }

    public onShowVotes() {
        this.socket.on('onShowVotes', (data: any) => {
            const pm = Container.get(PlayerManager);
            pm.showResults(data);
        });
    }

    public onReset() {
        this.socket.on('onReset', () => {
            const pm = Container.get(PlayerManager);
            pm.reset();
        });
    }

    public onPosition() {
        this.socket.on('onPosition', (data: ITransferObject) => {
            if (data.entityType === ENTITY_TYPES.Player) {
                // console.log(data);
                const pm = Container.get(PlayerManager);
                pm.changeEntity(plainToClass(TransferObject, data));
                return;
            }

            const lm = Container.get(EntityManager);
            lm.changeEntity(plainToClass(TransferObject, data));
        });
    }

    public onShot() {
        this.socket.on('playerShot', (obj: any) => {
            const pm = Container.get(PlayerManager);
            pm.shot(obj);
            const em = Container.get(EntityManager);
            em.createEntity(obj);
        });
    }

    public onSnowDestroy() {
        this.socket.on('snowBallDestroy', (obj: any) => {
            const em = Container.get(EntityManager);
            em.removeEntity(obj);
            // console.log(obj.id);
        });
    }

    public onDamage() {
        this.socket.on('damage', (obj: any) => {
            const pm = Container.get(PlayerManager);
            pm.damage(obj);
        });
    }

    @Speaker
    public onMap() {
        return this.socket.on('onMap', (obj: any) => {
            this.map = obj;
        });
    }

    // --------- Emitters ------------------

    public sendName(name: string) {
        this.socket.emit('enterName', {playerName: name});
    }

    public createRoom(name: string) {
        this.socket.emit('createRoom', {roomName: name});
    }

    public joinRoom(name: string) {
        this.socket.emit('joinRoom', name);
    }

    public vote(sp: number) {
        this.socket.emit('vote', sp);
    }

    public showVotes() {
        this.socket.emit('showVotes');
    }

    public reset() {
        this.socket.emit('onReset');
    }

    public setPointPlacement(point: Point) {
        this.socket.emit('onNewPlacement', point);
    }

    public sendKeyPressed(bitMask: number) {
        this.socket.emit('keyPressed', bitMask);
    }

    public changeOwner(id: string) {
        this.socket.emit('changeOwner', id);
    }
}
