import Texture = PIXI.Texture;
import {Player} from '../Player';
import AnimatedSprite = PIXI.extras.AnimatedSprite;
import DisplayObject = PIXI.DisplayObject;
import {Network} from '../Network';
import {Container} from 'typedi';
import {AbstractState} from './help/AbstractState';
import {Point} from '../helpers/cyber/Point';

export class ShotState extends AbstractState {

    public name = 'shot';

    public textures: Texture[] = [];
    public player: Player;

    private texturesPack: any = {
        throw1: {
            textures: [],
            speed   : 1,
            active  : false,
        },
        throw2: {
            textures: [],
            speed   : 0.5,
            active  : false,
        },
    };

    constructor(player: Player) {
        super();
        this.player = player;

        for (let i = 0; i <= 156; i++) {
            let str = 'throw0';
            if (i < 10) {
                str += '0';
            }
            if (i < 100) {
                str += '0';
            }
            str += i + '.png';
            const texture = PIXI.Texture.fromFrame(str);
            this.texturesPack.throw1.textures.push(texture);
        }

        for (let i = 20000; i <= 20115; i++) {
            let str       = 'throw';
            str += i + '.png';
            const texture = PIXI.Texture.fromFrame(str);
            this.texturesPack.throw2.textures.push(texture);
        }
    }

    public getTextures() {
        return this.texturesPack.throw2.textures;
    }

    public run() {

    }

    public idle() {

    }

    public death() {

    }

    public shot() {
        this.sendKeys(0);
        const playerSprite: AnimatedSprite | DisplayObject = this.player.container.getChildByName('player');

        playerSprite.rotation = this.player.endMarker.angle - Math.PI / 2; // Math.atan2(0, -10);
        this.player.angle     = playerSprite.rotation;
        playerSprite.scale.set(1);
        const v               = new Point(Math.cos(playerSprite.rotation + Math.PI / 2), Math.sin(playerSprite.rotation + Math.PI / 2));
        playerSprite.position = v.Multiply(80);

        if (playerSprite instanceof AnimatedSprite) {
            playerSprite.onComplete = () => {
                // this.player.setPosition(new Point(100,0));
                this.player.changeState(this.player.states.idle);
                this.player.activeState.idle();
            };
        }
    }

    public sendKeys(bitMask: number) {
        const network = Container.get(Network);
        network.sendKeyPressed(0);
    }
}
