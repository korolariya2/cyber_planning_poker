import {Player} from '../Player';
import Texture = PIXI.Texture;
import {Network} from '../Network';
import {Container} from 'typedi';
import AnimatedSprite = PIXI.extras.AnimatedSprite;
import DisplayObject = PIXI.DisplayObject;
import {AbstractState} from './help/AbstractState';
import {Tools} from '../helpers/Tools';
import {log} from 'util';

export class IdleState extends AbstractState {

    public name = 'idle';

    private player: Player;

    private texturesPack: any = {
        idle1: {
            textures: [],
            speed   : .5,
            active  : false,
        },
        idle2: {
            textures: [],
            speed   : 0.5,
            active  : false,
        },
    };

    constructor(player: Player) {
        super();
        this.player = player;
        for (let i = 10000; i <= 10098; i++) {
            const str     = 'idle' + i + '.png';
            const texture = PIXI.Texture.fromFrame(str);
            this.texturesPack.idle1.textures.push(texture);
        }
        for (let i = 20000; i <= 20120; i++) {
            const str     = 'idle' + i + '.png';
            const texture = PIXI.Texture.fromFrame(str);
            this.texturesPack.idle2.textures.push(texture);
        }
    }

    public getTextures() {
        const pack = this.texturesPack['idle' + Tools.getRandom(1, 2)];
        this.setActive(pack);
        return pack.textures;
    }

    public death(): void {
        this.player.changeState(this.player.states.death);
        this.player.activeState.death();
    }

    public idle(): void {
        const playerSprite: AnimatedSprite | DisplayObject = this.player.container.getChildByName('player');
        if (playerSprite instanceof AnimatedSprite) {
            const config                = this.getActiveAnimation();
            playerSprite.animationSpeed = config.speed;
            playerSprite.rotation       = this.player.angle;
            playerSprite.onComplete     = () => {
                if (this.player.activeState === this) {
                    this.player.changeState(this.player.states.idle);
                    this.player.activeState.idle();
                }
            };
        }
    }

    public walk(): void {
        if (this.player.lengthPath > 2) {
            this.player.changeState(this.player.states.walk);
            this.player.activeState.walk();
        }
    }

    public run(): void {
        if (this.player.lengthPath > 2) {
            this.player.changeState(this.player.states.run);
        }
    }

    public shot() {
        this.player.changeState(this.player.states.shot);
        this.player.activeState.shot();
    }

    public sendKeys(bitMask: number) {
        // console.log('sendKeys idle ' + bitMask);
        const network = Container.get(Network);
        network.sendKeyPressed(bitMask);
    }

    private setActive(pack: any) {
        Object.keys(this.texturesPack).map((key) => {
            this.texturesPack[key].active = false;
        });
        pack.active = true;
    }

    private getActiveAnimation() {
        return Object.keys(this.texturesPack).map((key) => {
            if (this.texturesPack[key].active) {
                return this.texturesPack[key];
            }
        }).filter(item => item)[0];
    }
}
