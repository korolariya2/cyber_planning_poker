import {Player} from '../Player';
import Texture = PIXI.Texture;
import DisplayObject = PIXI.DisplayObject;
import AnimatedSprite = PIXI.extras.AnimatedSprite;
import {Network} from '../Network';
import {Container} from 'typedi';
import {AbstractState} from './help/AbstractState';

export class WalkState extends AbstractState {

    public name = 'walk';

    public textures: Texture[] = [];

    private player: Player;

    private active = false;

    private texturesPack: any = {
        walk1: {
            textures: [],
            speed   : .5,
            active  : false,
        },
    };

    constructor(player: Player) {
        super();
        this.player = player;
        for (let i = 0; i <= 31; i++) {
            let str = 'walk00';
            if (i < 10) {
                str += '0';
            }
            str += i + '.png';
            const texture = PIXI.Texture.fromFrame(str);
            this.texturesPack.walk1.textures.push(texture);
        }
    }

    public getTextures() {
        return this.texturesPack.walk1.textures;
    }

    public walk() {
        if (this.player.lengthPath <= 2) {
            this.player.activeState.idle();
            return;
        }
        const playerSprite: AnimatedSprite | DisplayObject = this.player.container.getChildByName('player');

        if (playerSprite instanceof AnimatedSprite && !playerSprite.loop) {
            playerSprite.loop = true;
        }
        this.changeSpeedAnimation();
        this.player.rotate(this.player.direction);
    }

    public death(): void {
        this.player.changeState(this.player.states.death);
        this.player.activeState.death();
    }

    public idle(): void {
        this.player.changeState(this.player.states.idle);
        this.player.activeState.idle();
        this.player.rotate(this.player.direction);
    }

    public run(): void {
        if (this.player.lengthPath <= 2) {
            this.player.activeState.idle();
            return;
        }
        // this.changeSpeedAnimation();
        this.player.rotate(this.player.direction);
    }

    public shot() {
        this.player.changeState(this.player.states.idle);
        this.player.activeState.shot();
        // const playerSprite: AnimatedSprite | DisplayObject = this.player.container.getChildByName('player');
        // if (playerSprite instanceof AnimatedSprite) {
        //     playerSprite.animationSpeed = 0.1;
        //     playerSprite.scale.set(1);
        // }
    }

    public sendKeys(bitMask: number) {
        const network = Container.get(Network);
        network.sendKeyPressed(bitMask);
    }

    private changeSpeedAnimation() {
        const playerSprite: AnimatedSprite | DisplayObject = this.player.container.getChildByName('player');
        if (playerSprite instanceof AnimatedSprite) {
            playerSprite.animationSpeed = (this.player.lengthPath / 20 < .5) ? .5 : this.player.lengthPath / 20;
        }
    }
}
