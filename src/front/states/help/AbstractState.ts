import {State} from './state';

export abstract class AbstractState implements State {

    public name: string;

    protected textures: PIXI.Texture[] = [];

    public death(): void {
    }

    public idle(): void {
    }

    public run(): void {
    }

    public sendKeys(bit: number): void {
    }

    public shot(): void {
    }

    public walk(): void {
    }

    public getTextures() {
        return this.textures;
    }
}
