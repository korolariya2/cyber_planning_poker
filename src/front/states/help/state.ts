export interface State {

    name: string;

    run(): void;

    idle(): void;

    death(): void;

    shot(): void;

    sendKeys(bit: number): void;
}
