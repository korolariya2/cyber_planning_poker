import {State} from './help/state';
import {Player} from '../Player';
import Texture = PIXI.Texture;
import AnimatedSprite = PIXI.extras.AnimatedSprite;
import DisplayObject = PIXI.DisplayObject;
import {AbstractState} from './help/AbstractState';
import {Tools} from '../helpers/Tools';

export class DeathState extends AbstractState {

    public name = 'death';

    private player: Player;

    private texturesPack: any = {
        death1: {
            textures: [],
            speed   : 0.5,
            active  : false,
        },
    };

    constructor(player: Player) {
        super();
        this.player = player;
        for (let i = 0; i <= 91; i++) {
            let str = 'death0';
            if (i < 10) {
                str += '0';
            }
            if (i < 100) {
                str += '0';
            }
            str += i + '.png';
            const texture = PIXI.Texture.fromFrame(str);
            this.texturesPack.death1.textures.push(texture);
        }
    }

    public death(): void {
        const playerSprite: AnimatedSprite | DisplayObject = this.player.container.getChildByName('player');
        playerSprite.rotation       = this.player.angle;
        if (playerSprite instanceof AnimatedSprite) {
            const config = this.getActiveAnimation();
            playerSprite.scale.set(1);
            playerSprite.animationSpeed = config.speed;
            playerSprite.loop           = false;
            playerSprite.onComplete     = () => {
                if (this.player.activeState === this) {
                    this.player.changeState(this.player.states.idle);
                    this.player.activeState.idle();
                }
            };
        }
    }

    public idle(): void {

    }

    public run(): void {
        // this.player.changeState(this.player.states.run);
    }

    public shot() {

    }

    public sendKeys() {

    }

    public getTextures() {
        const pack = this.texturesPack['death1'];
        this.setActive(pack);
        return pack.textures;
    }

    private setActive(pack: any) {
        Object.keys(this.texturesPack).map((key) => {
            this.texturesPack[key].active = false;
        });
        pack.active = true;
    }

    private getActiveAnimation() {
        return Object.keys(this.texturesPack).map((key) => {
            if (this.texturesPack[key].active) {
                return this.texturesPack[key];
            }
        }).filter(item => item)[0];
    }
}
