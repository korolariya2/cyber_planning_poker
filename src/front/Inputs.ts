import {Container, Service} from 'typedi';
import {Network} from './Network';
import {HtmlInteractive} from './helpers/HtmlInteractive';
import {Keys} from './helpers/Keys';
import {PlayerManager} from './PlayerManager';

@Service()
export class Inputs {

    public elements: HtmlInteractive = new HtmlInteractive();

    public player: any = {
        name: '',
        room: '',
    };

    public keys = new Keys();

    public network: Network;

    constructor() {
        this.getElements();
        this.listeners();

        this.network = Container.get(Network);

        setInterval(() => {
            const pm          = Container.get(PlayerManager);
            const foundPlayer = pm.findAvatar(this.network.socket.id);
            if (foundPlayer) {
                foundPlayer.activeState.sendKeys(this.keys.constructInputBitmask());
            }
        }, 100);
    }

    public getElements() {
        this.elements.setRom    = document.getElementById('setRoom');
        this.elements.enterName = document.getElementById('enterName');
        this.elements.submit    = document.getElementById('submit');
        this.elements.viewVotes = document.getElementById('viewVotes');
        this.elements.reset     = document.getElementById('reset');
    }

    public listeners() {
        this.onSubmit();
        this.onChangeName();
        this.onChangeOwnerRoom();
        this.keys.attachEvents();
    }

    public onChangeOwnerRoom() {
        this.elements.setRom.addEventListener('change', (event: any) => {
            this.player.room = event.target.value;
        });
    }

    public onChangeName() {
        this.elements.enterName.addEventListener('change', (event: any) => {
            this.player.name = event.target.value;
            const network    = Container.get(Network);
            network.sendName(this.player.name);
        });
    }

    public onSubmit() {
        this.elements.submit.addEventListener('click', (event: MouseEvent) => {
            if (!this.player.name || !this.player.room) {
                alert('Enter your name and room!');
                return;
            }
            const network = Container.get(Network);

            network.createRoom(this.player.room);
        });

        this.elements.viewVotes.addEventListener('click', (event: MouseEvent) => {
            const network = Container.get(Network);
            network.showVotes();
        });

        this.elements.reset.addEventListener('click', (event: MouseEvent) => {
            const network = Container.get(Network);
            network.reset();
        });
    }
}
