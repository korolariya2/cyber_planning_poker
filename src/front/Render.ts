import {Container, Service} from 'typedi';
import {PlayerManager} from './PlayerManager';
import {EntityManager} from './EntityManager';
import {PlayersList} from './entities/PlayersList';
import {Point} from './helpers/cyber/Point';
import {Network} from './Network';
import Layer = PIXI.display.Layer;

@Service()
export class Render {
    public app: PIXI.Application;
    public width: number;
    public height: number;
    public resources: any;
    public world: Layer = new PIXI.display.Layer();
    public ui: Layer    = new PIXI.display.Layer();

    public playerManager: PlayerManager;
    public entityManager: EntityManager;

    constructor() {
        PIXI.loader
            .add('avatar', '/assets/avatar.png')
            .add('cards', '/assets/cards.json')
            .add('player', '/assets/player.json')
            .add('death1', '/assets/death1.json')
            .add('punch', '/assets/punch.json')
            .add('leaf1', '/assets/leafs/leaf1.png')
            .add('leaf2', '/assets/leafs/leaf2.png')
            .add('leaf3', '/assets/leafs/leaf3.png')
            .add('snow', '/assets/snowflake/snow.png')
            .add('run', '/assets/run.json')
            .add('idle1', '/assets/idle1.json')
            .add('idle2', '/assets/idle2.json')
            .add('walk', '/assets/walk1.json')
            .add('throw1', '/assets/throw1.json')
            .add('throw2', '/assets/throw2.json')
            .load(this.onLoaded.bind(this));
    }

    public update(delta: number) {
        this.playerManager.players.forEach((player) => {
            player.update(delta);
        });
        this.entityManager.update(delta);
    }

    public onLoaded(loader: any, res: any) {
        this.resize();
        this.app = new PIXI.Application(this.width, this.height, {
            backgroundColor: 0x3f51b5,
            antialias      : true,
            autoResize     : true,
        }, false);
        document.getElementById('wrapper').appendChild(this.app.view);
        this.app.stop();
        this.app.stage                  = new PIXI.display.Stage();
        this.app.stage.addChild(this.world);
        this.app.stage.addChild(this.ui);

        this.resources     = res;
        this.playerManager = Container.get(PlayerManager);
        this.entityManager = Container.get(EntityManager);

        this.world.position.x = this.width / 2;
        this.world.position.y = this.height / 2;

        const gr = new PIXI.Graphics();
        // gr.zIndex = 1;
        gr.lineStyle(1, 0x9e9e9e, 0.1);
        for (let i = -1000; i < 1000; i++) {
            gr.moveTo(-25000, i * 50);
            gr.lineTo(25000, i * 50);
            gr.moveTo(i * 50, -25000);
            gr.lineTo(i * 50, 25000);
        }
        gr.endFill();

        this.world.interactive = true;
        this.world.group.enableSort = true;
        this.ui.group.enableSort = true;

        let [x, y]       = [0, 0];
        let [preX, preY] = [0, 0];

        this.world.on('pointermove', (e: any) => {
            const posG     = this.world.getGlobalPosition();
            const posClick = e.data.global;
            const click    = new Point(posClick.x - posG.x, posClick.y - posG.y);
            x              = click.x - (click.x % 50);
            y              = click.y - (click.y % 50);

            x += (x < 0) ? -50 : 0;
            y += (y < 0) ? -50 : 0;
            x += (x === 0 && click.x < 0) ? -50 : 0;
            y += (y === 0 && click.y < 0) ? -50 : 0;

            if (preX === x && preY === y) {
                return;
            }

            const select = new PIXI.Graphics();
            select.lineStyle(1, 0xFF9800, 1);
            select.moveTo(x, y);
            select.lineTo(x + 50, y);
            select.lineTo(x + 50, y + 50);
            select.lineTo(x, y + 50);
            select.lineTo(x, y);

            select.endFill();
            select.name = 'select';

            const name = this.world.getChildByName('select');
            if (name) {
                this.world.removeChild(name);
            }
            this.world.addChild(select);
            [preX, preY] = [x, y];

            const net = Container.get(Network);
            net.setPointPlacement(click);
        });

        gr.zIndex = 0;

        this.world.addChild(gr);

        const meter = new FPSMeter();
        this.app.start();
        this.app.ticker.add((delta: number) => {
            meter.tick();
            this.update(delta);
        });
    }

    private resize() {
        this.width      = document.getElementById('wrapper').offsetWidth;
        this.height     = document.getElementById('wrapper').offsetHeight;
        window.onresize = (event) => {
            this.width  = document.getElementById('wrapper').offsetWidth;
            this.height = document.getElementById('wrapper').offsetHeight;
            this.app.renderer.resize(this.width, this.height);
            const pl = Container.get(PlayersList);
            pl.updatePosition();
        };
    }
}
