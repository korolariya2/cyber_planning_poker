import {Container, Service} from 'typedi';
import {Player} from './Player';
import {Deck} from './entities/Deck';
import {Card} from './entities/Cards';
import {TransferObject} from './helpers/DataTransfer';
import {Point} from './helpers/cyber/Point';
import {PlayersList} from './entities/PlayersList';

@Service()
export class PlayerManager {

    public players: Player[] = [];

    public deck: Deck;

    public results: Card[] = [];

    constructor() {
    }

    public createPlayers(data: Array<{ id: string, name: string, owner: boolean }>) {
        data.forEach((item) => {
            this.createPlayer(item);
            this.setOwner(item);
            this.placement(item);
        });
    }

    public removeOwner() {
        this.players.forEach((player) => {
            player.owner = false;
        });
    }

    public setOwner(item: { id: string, name: string, owner: boolean }) {
        if (item.owner) {
            this.removeOwner();
            const ownerPlayer = this.players.find(player => player.id === item.id);
            ownerPlayer.owner = true;
            const pl          = Container.get(PlayersList);
            pl.removeOwners();
            setTimeout(() => {
                pl.addOwner(item.id);
            });
        }
    }

    public placement(item: any = null) {
        if (item.position) {
            const player = this.players.find(itemPlayer => itemPlayer.id === item.id);
            if (player) {
                player.setPosition(item.position);
                return;
            }
        }
    }

    public createPlayer(data: { id: string, name: string, owner: boolean }) {
        let player = this.players.find(itemPlayer => itemPlayer.id === data.id);

        if (player) {
            return;
        }
        player = Object.assign(new Player(), data);
        this.players.push(player);
    }

    public createDeck() {
        this.deck = new Deck();
    }

    public removePlayer(data: string[]) {
        this.players.forEach((player, index) => {
            if (data.indexOf(player.id) === -1) {
                player.destroy();
                this.players.splice(index, 1);
            }
        });
    }

    public checkVotedPlayers(data: any) {
        this.players.forEach((player) => {
            player.disableFilter();
            if (data.indexOf(player.id) !== -1) {
                player.enableFilter();
            }
        });
    }

    public showResults(data: any) {
        data.forEach((item: any, index: number) => {
            const alias = Object.keys(this.deck.deck).find((key) => {
                return this.deck.deck[key] === item.sp;
            });
            const card  = new Card(alias);
            card.setPosition(new Point(100 + 100 * index, 100));
            card.addText(item.name);
            this.results.push(card);
        });
    }

    public reset() {
        this.results.forEach((card) => {
            card.destroy();
        });
        this.deck.reset();
        this.players.forEach((player) => {
            player.disableFilter();
        });
    }

    public changeEntity(data: TransferObject) {
        const player = this.players.find(itemPlayer => itemPlayer.id === data.id);
        if (player) {
            player.addMarker(data);
        }
    }

    public updatePlayerName(data: { id: string, name: string }) {
        const player = this.players.find(itemPlayer => itemPlayer.id === data.id);
        if (player) {
            player.updateName(data.name);
        }
    }

    public shot(obj: { socket_id: string }) {
        const player = this.players.find(itemPlayer => itemPlayer.id === obj.socket_id);
        if (player) {
            player.shot();
        }
    }

    public findAvatar(id: string) {
        return this.players.find(itemPlayer => itemPlayer.id === id);
    }

    public damage(obj: any) {
        const player = this.players.find(itemPlayer => itemPlayer.id === obj.player_id);
        if (!player) {
            return;
        }
        player.damage();
    }
}
