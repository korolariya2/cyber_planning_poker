import {Point} from '../helpers/cyber/Point';
import {TransferObject} from '../helpers/DataTransfer';
import {IEntity} from '../interfaces/IEntity';
import {Render} from '../Render';
import {Container} from 'typedi';

export abstract class Entity implements IEntity {

    public id: string;

    public container: PIXI.Container = new PIXI.Container();

    public setPosition(point: PIXI.Point) {
        this.container.position.set(point.x, point.y);
    }

    public getPosition(): Point {
        return new Point(this.container.x, this.container.y);
    }

    public update(delta: number) {
    }

    public addMarker(marker: TransferObject) {

    }

    public destroy() {
        const render = Container.get(Render);
        render.world.removeChild(this.container);
    }
}