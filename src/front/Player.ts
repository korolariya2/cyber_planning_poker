import {Container} from 'typedi';
import {Render} from './Render';
import * as filters from 'pixi-filters';
import AnimatedSprite = PIXI.extras.AnimatedSprite;
import DisplayObject = PIXI.DisplayObject;
import {IdleState} from './states/Idle';
import {RunState} from './states/run';
import {DeathState} from './states/death';
import {ShotState} from './states/shot';
import {Tools} from './helpers/Tools';
import {Point} from './helpers/cyber/Point';
import {TransferObject} from './helpers/DataTransfer';
import {VPoint} from './helpers/cyber/VPoint';
import {PlayersList} from './entities/PlayersList';
import {Entity} from './abstracts/Entity';
import {Network} from './Network';
import {Inputs} from './Inputs';
import {AbstractState} from './states/help/AbstractState';
import {WalkState} from './states/walk';
import {Map} from './entities/Map';

export class Player extends Entity {
    public container: PIXI.Container;
    public inputs: Inputs;
    public states: any;

    public activeState: AbstractState;

    public positions: TransferObject[] = [];

    public lengthPath: number;

    public startMarker: TransferObject;
    public endMarker: TransferObject = {
        id        : '0',
        angle     : 0,
        position  : new Point(),
        entityType: 0,
        time      : 0,
    };

    public angle = 0;

    public direction: VPoint;

    public map: Map = new Map();

    private _name: string;

    private _id: string;

    private _owner: boolean;

    private render: Render;

    private deltaFps: number = 0;

    private time   = 0;
    private speed  = 0;
    private length = 0;

    private prePos = new Point();

    set id(id: string) {
        this.container.name = 'Player' + id;
        this._id            = id;
    }

    get id() {
        return this._id;
    }

    set name(name: string) {
        const pl = Container.get(PlayersList);
        pl.addName(name, this.id);
        this._name = name;
        this.loadAvatar();
    }

    get name() {
        return this._name;
    }

    set owner(owner: boolean) {
        this._owner = owner;

        if (owner) {
            this.addCrown();
            return;
        }
        this.removeCrown();
    }

    get owner() {
        return this._owner;
    }

    constructor() {
        super();
        this.container = new PIXI.Container();

        const net = Container.get(Network);
        net.addListener('onMap', (data: any) => {
            if (this.id !== net.socket.id) {
                return;
            }
            const pos      = this.getPosition();
            const x        = Math.ceil(pos.x / 50) + 500;
            const y        = Math.ceil(pos.y / 50) + 500;
            const map: any = [];
            const offset   = Math.ceil(this.map.size / 2);
            for (let i = 0; i < this.map.size; i++) {
                map[i] = [];
                for (let j = 0; j < this.map.size; j++) {

                    const found = data.find((el: any) => {
                        return el.x === x - offset + i && el.y === y - offset + j;
                    });
                    if (found) {
                        map[i][j] = found;
                    }
                }
            }
            this.map.createMap(map);
        });
    }

    public loadPlayer() {
        this.states = {
            idle : new IdleState(this),
            run  : new RunState(this),
            walk : new WalkState(this),
            death: new DeathState(this),
            shot : new ShotState(this),
        };

        this.changeState(this.states.idle);
        this.activeState.idle();
    }

    public shot() {
        this.activeState.shot();
    }

    public changeState(state: any) {
        const player: AnimatedSprite | DisplayObject = this.container.getChildByName('player');
        if (player instanceof AnimatedSprite) {
            player.stop();
            player.destroy();
            this.container.removeChild(player);
        }

        this.activeState   = state;
        const playerSprite = new PIXI.extras.AnimatedSprite(this.activeState.getTextures());
        if (playerSprite instanceof AnimatedSprite) {
            playerSprite.name = 'player';
            playerSprite.anchor.set(0.5);
            playerSprite.scale.set(0.5);
            playerSprite.loop = false;

            this.container.addChild(playerSprite);
            this.container.zIndex = 1;
            this.addCrown();

            playerSprite.gotoAndPlay(0);
        }
    }

    public loadAvatar() {
        this.render = Container.get(Render);
        this.drawName();
        const shadow           = new filters.DropShadowFilter();
        this.container.filters = [shadow];
        this.loadPlayer();
        this.render.world.addChild(this.container);
    }

    public addCrown() {
        if (this._owner) {
            const player   = this.container.getChildByName('player');
            const outline  = new filters.OutlineFilter(2, 0xFF8800);
            player.filters = [outline];
        }
    }

    public removeCrown() {
        const player   = this.container.getChildByName('player');
        player.filters = [];
    }

    public getPosition(): Point {
        return new Point(this.container.x, this.container.y);
    }

    public addMarker(data: TransferObject) {
        if (!data.id) {
            return;
        }
        this.positions.push(data);

        const [one, two] = this.positions;
        if (two) {
            this.deltaFps    = 0;
            this.startMarker = this.positions.shift();
            this.endMarker   = two;

            this.time      = this.endMarker.time - this.startMarker.time;
            this.direction = this.startMarker.position.VectorTo(this.endMarker.position);
            this.length    = this.direction.length();
            this.speed     = this.length / this.time;

            // this.rotate(this.direction);

            setTimeout(() => {
                this.checkChangePos(this.startMarker.position, this.endMarker.position);
            });
        }
    }

    public checkChangePos(start: Point, end: Point) {
        this.lengthPath = start.VectorTo(end).length();
        this.activeState.walk();
    }

    public rotate(p: VPoint) {
        const player    = this.container.getChildByName('player');
        player.rotation = Math.atan2(p.y, p.x) - Math.PI / 2;
        this.angle      = player.rotation;
    }

    public update(delta: number) {
        this.focusToPoint();

        this.deltaFps += delta;
        if (!this.endMarker) {
            return;
        }

        if (this.length > 0) {
            const i = this.deltaFps * this.speed / this.length;
            this.setPosition(Tools.lerp(this.getPosition(), this.endMarker.position, i));
        }

        this.selectCell();
    }

    public destroy() {
        const render = Container.get(Render);
        render.world.removeChild(this.container);
        const pl = Container.get(PlayersList);
        pl.removeName(this.id);
    }

    // public addDefaultFilters() {
    //     const shadow = new filters.DropShadowFilter();
    //     this.container.filters.push(shadow);
    // }

    public enableFilter() {
        const outlineFilterBlue = new filters.OutlineFilter(2, 0x99ff99);
        const name              = this.container.getChildByName('playerName');
        name.filters            = [outlineFilterBlue];
        const pl                = Container.get(PlayersList);
        pl.vote(this.id);
    }

    public disableFilter() {
        const name   = this.container.getChildByName('playerName');
        name.filters = [];
        const pl     = Container.get(PlayersList);
        pl.unvote(this.id);
    }

    public drawName() {
        const style    = new PIXI.TextStyle({
            fontFamily     : 'Arial',
            fontSize       : 18,
            fontStyle      : 'italic',
            fontWeight     : 'bold',
            stroke         : '#1a6f1d',
            strokeThickness: 5,
        });
        const nameText = new PIXI.Text(this._name, style);
        nameText.name  = 'playerName';
        nameText.anchor.set(0.5);
        nameText.y = 30;

        this.container.addChild(nameText);
    }

    public selectCell() {
        const pos = this.getPosition();

        let [x, y] = [0, 0];
        x          = pos.x - (pos.x % 50);
        y          = pos.y - (pos.y % 50);
        x += (x < 0) ? -50 : 0;
        y += (y < 0) ? -50 : 0;
        x += (x === 0 && this.getPosition().x < 0) ? -50 : 0;
        y += (y === 0 && this.getPosition().y < 0) ? -50 : 0;

        if (this.prePos.x === x && this.prePos.y === y) {
            return;
        }

        const select = new PIXI.Graphics();
        select.lineStyle(1, 0xFF9800, 1);
        // select.drawRoundedRect(x, y, 50, 50, 0);
        select.moveTo(x, y);
        select.lineTo(x + 50, y);
        select.lineTo(x + 50, y + 50);
        select.lineTo(x, y + 50);
        select.lineTo(x, y);

        select.endFill();
        select.name   = 'select' + this.id;
        select.zIndex = 0;

        const name = this.render.world.getChildByName(select.name);
        if (name) {
            this.render.world.removeChild(name);
        }
        this.render.world.addChild(select);
        [this.prePos.x, this.prePos.y] = [x, y];
    }

    public updateName(name: string) {
        this._name = name;
        const text = this.container.getChildByName('playerName');
        this.container.removeChild(text);
        this.drawName();
    }

    public setPosition(point: Point) {
        this.container.position.set(point.x, point.y);
    }

    public damage() {
        this.changeState(this.states.death);
        this.activeState.death();
    }

    private focusToPoint() {
        const net = Container.get(Network);
        if (this.id !== net.socket.id) {
            return;
        }
        const plPos = this.getPosition();
        const point = {
            x: this.render.width / 2 - plPos.x,
            y: this.render.height / 2 - plPos.y,
        };

        const x = point.x - this.render.world.position.x;
        const y = point.y - this.render.world.position.y;

        const len = Math.sqrt(x * x + y * y);
        if (len) {
            this.render.world.position.x += ~~(len / 50) * x / len;
            this.render.world.position.y += ~~(len / 50) * y / len;
        }
    }

}
