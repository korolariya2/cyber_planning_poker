import {TransferObject} from '../helpers/DataTransfer';

export interface IEntity {
    id: string;

    addMarker(marker: TransferObject): void;

    update(delta: number): void;

    destroy(): void;
}