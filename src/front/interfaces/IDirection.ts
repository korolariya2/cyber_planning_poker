import {IPoint} from './IPoint';

export interface IDirection {
    [key: string]: IPoint;
}
