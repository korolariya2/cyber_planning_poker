export interface IBitmask {
    [key: string]: number;
}
