export interface ICode {
    [key: string]: string;
}
