import {Container, Service} from 'typedi';
import {Leaf} from './entities/Leaf';
import {TransferObject} from './helpers/DataTransfer';
import {Player} from './Player';
import {PlayersList} from './entities/PlayersList';
import {IEntity} from './interfaces/IEntity';
import {Table} from './entities/Table';

@Service()
export class EntityManager {
    public entities: IEntity[]       = [];
    public clientEntities: IEntity[] = [];

    public entityTypes: any = {
        1: Player,
        2: Leaf,
    };

    constructor() {
        const pl = Container.get(PlayersList);
        this.clientEntities.push(pl);
        this.createTable();
    }

    public createTable() {
        this.clientEntities.push(new Table());
    }

    public update(delta: number) {
        this.entities.forEach((entity) => {
            entity.update(delta);
        });
        this.clientEntities.forEach((entity) => {
            entity.update(delta);
        });
    }

    public createEntity(data: TransferObject): IEntity {
        if (this.entityTypes.hasOwnProperty(data.entityType)) {
            const entity = new this.entityTypes[data.entityType](data.id, data.position);
            this.entities.push(entity);
            return entity;
        }
        return null;
    }

    public changeEntity(data: TransferObject) {
        const entity = this.entities.find(item => item.id === data.id);
        if (entity) {
            entity.addMarker(data);
            return;
        }
        this.createEntity(data);
    }

    public removeEntity(obj: { id: string, position: PIXI.Point }) {
        const index = this.entities.findIndex(item => item.id === obj.id);
        this.entities[index].destroy();
        this.entities.splice(index, 1);
    }
}
