import {Point} from './cyber/Point';
import {Type} from 'class-transformer';
import {IPoint} from '../interfaces/IPoint';

export interface ITransferObject {
    entityType: number;
    id: string;
    position: IPoint;
    time: number;
}

export class TransferObject implements ITransferObject {
    public entityType: number;
    public id: string;
    @Type(() => Point)
    public position: Point;
    public time: number;
    public angle: number;
}