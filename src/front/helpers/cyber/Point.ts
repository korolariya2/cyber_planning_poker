import {plainToClass} from 'class-transformer';
import {VPoint} from './VPoint';
import {IPoint} from '../../interfaces/IPoint';

export class Point extends PIXI.Point implements IPoint {
    public Multiply(value: number) {
        this.x *= value;
        this.y *= value;
        return this;
    }

    public Add(point: Point) {
        this.x += point.x;
        this.y += point.y;
        return this;
    }

    public clone(): Point {
        return plainToClass(Point, this);
    }

    public VectorTo(point: Point): VPoint {
        return plainToClass(VPoint, point.clone().Add(this.clone().Multiply(-1)));
    }
}
