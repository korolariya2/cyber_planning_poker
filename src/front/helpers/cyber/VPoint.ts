import {plainToClass} from 'class-transformer';

export class VPoint extends PIXI.Point {
    public clone(): VPoint {
        return plainToClass(VPoint, this);
    }

    public length(): number {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    }

    public Multiply(value: number) {
        this.x *= value;
        this.y *= value;
        return this;
    }
}
