import {Point} from './cyber/Point';

export class Tools {
    public static getRandom(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public static clamp01(t: number) {
        if (t > 1) {
            return 1;
        }
        if (t < 0) {
            return 0;
        }
        return t;
    }

    public static lerp(a: Point, b: Point, t: any): Point {
        t = Tools.clamp01(t);
        return new Point(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t);
    }
}
