export class HtmlInteractive {
    public setRom?: HTMLElement    = null;
    public enterName?: HTMLElement = null;
    public submit?: HTMLElement    = null;
    public viewVotes?: HTMLElement = null;
    public reset?: HTMLElement     = null;
}
