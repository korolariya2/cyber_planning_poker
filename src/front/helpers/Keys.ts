import {IKey} from '../interfaces/IKey';
import {ICode} from '../interfaces/ICode';
import {INPUT_BITMASK} from './Bitmask';
import {IDirection} from '../interfaces/IDirection';
import {SPoint} from '../../server/helpers/SPoint';

export class Keys {

    public keyPressed      = 0;
    public keyCodes: ICode = {
        16: 'shift',
        32: 'space',
        65: 'left',
        87: 'up',
        68: 'right',
        83: 'down',
    };

    public keys: IKey = {
        shift: false,
        space: false,
        up   : false,
        down : false,
        left : false,
        right: false,
        mouse: false,
    };

    public direction: IDirection = {
        left : new SPoint(-1, 0),
        right: new SPoint(1, 0),
        up   : new SPoint(0, -1),
        down : new SPoint(0, 1),
        shift: new SPoint(),
    };

    constructor() {
    }

    public attachEvents() {
        document.getElementById('wrapper').addEventListener('keydown', (e) => {
            this.keyDown(e);
        }, false);
        document.getElementById('wrapper').addEventListener('keyup', (e) => {
            this.keyUp(e);
        }, false);
    }

    public keyDown(e: any) {
        if (e.keyCode in this.keyCodes) {
            if (!this.keys[this.keyCodes[e.keyCode]]) {
                this.keyPressed++;
            }
            this.handler(e.keyCode, true);
            e.preventDefault();
        }
    }

    public keyUp(e: any) {
        if (e.keyCode in this.keyCodes) {
            this.handler(e.keyCode, false);
            this.keyPressed--;
            e.preventDefault();
        }
    }

    public handler(keyCode: any, enabled: boolean) {
        this.keys[this.keyCodes[keyCode]] = enabled;
    }

    public constructInputBitmask() {
        let input = 0;

        Object.keys(this.keys).forEach((key) => {
            if (this.keys[key]) {
                input |= INPUT_BITMASK[key.toUpperCase()];
            }
        });

        return input;
    }

    public deconstructInputBitmask(inputBitmask: number) {
        Object.keys(this.keys).forEach((key) => {
            this.keys[key] = Boolean(inputBitmask & INPUT_BITMASK[key.toUpperCase()]);
        });
    }

}
