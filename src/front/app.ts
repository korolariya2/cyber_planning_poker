import 'es6-shim';
import 'reflect-metadata';
import 'pixi.js';
import 'pixi-layers';
import {Container} from 'typedi';
import {Render} from './Render';
import {Network} from './Network';
import {Inputs} from './Inputs';

const app     = Container.get(Render);
const network = Container.get(Network);
const inputs  = Container.get(Inputs);

