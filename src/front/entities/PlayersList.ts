import {PlayerTitle} from './PlayerTitle';
import {Point} from '../helpers/cyber/Point';
import {Entity} from '../abstracts/Entity';
import {Container, Service} from 'typedi';
import {Render} from '../Render';

@Service()
export class PlayersList extends Entity {
    public id: string = 'playerList';

    public render: Render;
    public list: PlayerTitle[]                        = [];
    public names: Array<{ id: string, name: string }> = [];

    private currentPlace = new Point();

    constructor() {
        super();
        this.render = Container.get(Render);
        this.render.ui.addChild(this.container);
        this.createList();
    }

    public addName(name: string, id: string) {
        this.names.push({name: name, id: id});
        this.updateList();
    }

    public removeName(id: string) {
        const index = this.names.findIndex(item => item.id === id);
        if (index === -1) {
            return;
        }
        this.names.splice(index, 1);
        this.updateList();
    }

    public updateList() {
        this.clearList();
        this.createList();
    }

    public clearList() {
        this.list.forEach((item) => {
            this.container.removeChild(item.container);
        });
        this.list.splice(0, this.list.length);
    }

    public createList() {
        this.names.forEach((item) => {
            const row = new PlayerTitle(item);
            this.list.push(row);
        });

        this.list.forEach((item) => {
            this.container.addChild(item.container);
        });

        this.placement();
    }

    public placement() {
        let maxWidth = 0;
        this.list.forEach((item, index) => {
            const currentPos = item.getPosition();
            item.setPosition(new Point(currentPos.x, currentPos.y + index * (item.text.height + item.padding.y)));
            if (maxWidth < item.text.width) {
                maxWidth = item.text.width;
            }
        });

        this.list.map(item => item.drowBox(maxWidth));
    }

    public update() {
        this.updatePosition();
    }

    public updatePosition() {
        this.currentPlace = new Point(this.render.width - this.container.width - 20, 100);
        this.setPosition(this.currentPlace);
    }

    public vote(id: string) {
        this.list.find(item => item.id === id).select();
    }

    public unvote(id: string) {
        this.list.find(item => item.id === id).unselect();
    }

    public addOwner(id: string) {
        const found = this.list.find(title => title.id === id);
        if (found) {
            found.addOutline();
        }
    }

    public removeOwners() {
        this.list.forEach((title) => {
            title.removeOutline();
        });
    }
}
