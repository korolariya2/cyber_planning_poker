import {Card} from './Cards';
import {Render} from '../Render';
import {Container} from 'typedi';
import {Network} from '../Network';
import {Point} from '../helpers/cyber/Point';
import {Entity} from '../abstracts/Entity';

export class Deck extends Entity {
    public deck: any = {
        'zero'    : 0,
        'half'    : 0.5,
        'one'     : 1,
        'two'     : 2,
        'three'   : 3,
        'five'    : 5,
        'eight'   : 8,
        'thirteen': 13,
        'twenty'  : 20,
    };

    public cards: Card[] = [];

    public voted: Card;

    constructor() {
        super();
        this.createDeck();
    }

    public createDeck() {
        Object.keys(this.deck).forEach((item, index) => {
            const card   = new Card(item);
            const render = Container.get(Render);
            card.setPosition(new Point(100 + 100 * index, render.height - 230));
            this.setEvent(card);
            this.cards.push(card);
        });
    }

    public setEvent(card: Card) {
        card.getView().on('pointerdown', () => {
            this.vote(card);
        });
    }

    public vote(card: Card) {
        if (!this.voted) {
            const network = Container.get(Network);
            network.vote(this.deck[card.alias]);
            const render = Container.get(Render);
            card.setPosition(new Point(200, render.height / 2));
            card.selected = true;
            this.voted    = card;
        }
    }

    public reset() {
        this.voted = null;
        this.cards.forEach((card) => {
            card.destroy();
        });
        this.createDeck();
    }
}
