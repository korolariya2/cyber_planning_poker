import {Container} from 'typedi';
import {Render} from '../Render';
import * as filters from 'pixi-filters';
import TwistFilter = PIXI.filters.TwistFilter;
import {Tools} from '../helpers/Tools';
import {plainToClass} from 'class-transformer';
import {Point} from '../helpers/cyber/Point';
import {TransferObject} from '../helpers/DataTransfer';
import {Entity} from '../abstracts/Entity';
import {VPoint} from '../helpers/cyber/VPoint';

export class ShowBall extends Entity {
    public id: string;
    public container: PIXI.Container;

    public positions: TransferObject[] = [];

    public run = false;

    public filter: TwistFilter;

    public startMarker: TransferObject;

    public endMarker: TransferObject;

    private s = 0;

    private direction: any;
    private time: any;
    private length: any;
    private speed: any;

    private render: Render;

    private pointFilter = new Point(Tools.getRandom(10, 20), Tools.getRandom(10, 20));

    private deltaFps: number = 0;

    constructor(id: string, position = new PIXI.Point()) {
        super();
        this.id        = id;
        this.render    = Container.get(Render);
        this.container = new PIXI.Container();
        this.createLeaf();
        this.setPosition(position);
    }

    public createLeaf() {
        const render = Container.get(Render);
        const leaf   = PIXI.Sprite.fromImage('snow'); // Tools.getRandom(1, 4)

        leaf.scale.set(0.1);
        leaf.anchor.set(0.5);

        leaf.name = 'leaf';

        this.container.addChild(leaf);

        this.container.pivot.set(0.5);

        render.world.addChild(this.container);
    }

    public addMarker(marker: TransferObject) {
        this.positions.push(marker);
        const [one, two] = this.positions;
        if (two) {
            this.deltaFps    = 0;
            this.startMarker = this.positions.shift();
            this.endMarker   = two;

            this.time      = this.endMarker.time - this.startMarker.time;
            this.direction = this.startMarker.position.VectorTo(this.endMarker.position);
            this.length    = this.direction.length();
            this.speed     = this.length / this.time;

            this.rotate(two.angle);
        }
    }

    public rotate(r: number) {
        const player    = this.container.getChildByName('leaf');
        player.rotation = r; // Math.atan2(p.y, p.x);
    }

    public update(delta: number) {
        this.deltaFps += delta;
        if (!this.endMarker) {
            return;
        }
        if (this.length > 0) {
            const i = this.deltaFps * this.speed / this.length;
            this.setPosition(Tools.lerp(this.getPosition(), this.endMarker.position, i));
        }
    }

    private setFilterOffsetPos(point: Point) {
        this.filter.offset.x = point.x;
        this.filter.offset.y = point.y;
    }
}
