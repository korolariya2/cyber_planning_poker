import {Render} from '../Render';
import {Container} from 'typedi';
import {Entity} from '../abstracts/Entity';

export class Card extends Entity {
    public container: PIXI.Container;
    public alias: string;
    public selected = false;
    private render: Render;

    constructor(alias: string) {
        super();
        this.createCard(alias);
        this.render = Container.get(Render);
        this.container.zIndex = 1;
        this.render.ui.addChild(this.container);
    }

    public createCard(alias: string) {
        this.alias     = alias;
        this.container = new PIXI.Container();
        const card     = PIXI.Sprite.fromFrame(this.alias);
        card.name      = 'card';
        card.scale.set(0.4);
        card.interactive = true;
        card.buttonMode  = true;
        this.container.addChild(card);
        this.container.position.set(100, 100);
    }

    public getView() {
        return this.container.getChildByName('card');
    }

    public addText(text: string) {

        const style = new PIXI.TextStyle({
            fontFamily: 'Arial',
            fontSize  : 14,
            fontStyle : 'italic',
            fontWeight: 'bold',
        });

        const nameText = new PIXI.Text(text, style);
        nameText.anchor.set(0.5);
        nameText.position.x = this.container.width / 2;

        const ground = new PIXI.Graphics();
        ground.lineStyle(2, 0x000000, 1);
        ground.beginFill(0xB6B6B6, 1);
        ground.drawRect(0, 0, nameText.width + 20, nameText.height + 20);
        ground.position.x = this.container.width / 2;
        ground.pivot.x    = ground.width / 2;
        ground.pivot.y    = ground.height / 2;

        this.container.addChild(ground);
        this.container.addChild(nameText);
    }

    public destroy() {
        this.render.ui.removeChild(this.container);
    }
}
