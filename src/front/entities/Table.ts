import {Container} from 'typedi';
import {Render} from '../Render';
import {Network} from '../Network';
import {plainToClass} from 'class-transformer';
import {Point} from '../helpers/cyber/Point';
import {Entity} from '../abstracts/Entity';

export class Table extends Entity {
    public id = 'Table';
    public container: PIXI.Container;

    public radius = 400;

    constructor() {
        super();
        this.container.pivot.set(this.container.width / 2, this.container.height / 2);
        this.createTable();
    }

    public createTable() {
        const render   = Container.get(Render);
        const graphics = new PIXI.Graphics();
        graphics.beginFill(0x00bcd4, 0);
        graphics.lineStyle(1, 0xFFFFFF, 0.5);
        graphics.drawCircle(0, 0, this.radius);
        graphics.endFill();
        graphics.name = 'table';

        const centerCircle = new PIXI.Graphics();
        centerCircle.lineStyle(1, 0xFFFFFF, 0.5);
        centerCircle.drawCircle(0, 0, this.radius / 2);
        centerCircle.endFill();
        this.container.addChild(centerCircle);

        this.container.addChild(graphics);

        const lines = [
            {
                value: 0,
                label: '0°',
            },
            {
                value: -Math.PI / 9,
                label: '20°',
            },
            {
                value: -Math.PI / 6,
                label: '30°',
            },
            {
                value: -Math.PI / 4,
                label: '45°',
            },
            {
                value: -Math.PI / 3,
                label: '60°',
            }, {
                value: -Math.PI / 2,
                label: '90°',
            },
        ];
        const style = new PIXI.TextStyle({
            fontFamily: 'Arial',
            fontSize  : 20,
            fill      : '#FFFFFF',
        });

        for (const item of lines) {
            const line = new PIXI.Graphics();
            line.lineStyle(1, 0xFFFFFF, 0.5);
            line.moveTo(0, 0);
            let [x, y] = this.pointInCircle(item.value, 25000);
            line.lineTo(x, y);
            this.container.addChild(line);

            const label = new PIXI.Text(item.label, style);
            [x, y]      = this.pointInCircle(item.value, 400);
            label.x     = x;
            label.y     = y;
            this.container.addChild(label);
        }

        this.container.interactive = true;

        this.container.alpha = 0.5;

        this.container.on('pointermove', (e: any) => {
            // const posG  = this.container.getGlobalPosition();
            // const posC  = e.data.global;
            // const click = new Point(posC.x - posG.x, posC.y - posG.y);
            // const net   = Container.get(Network);
            // net.setPointPlacement(click);
        });

        render.world.addChild(this.container);

        this.setPosition(new Point(0, 0));
    }

    public setPosition(point: Point) {
        this.container.position.set(point.x, point.y);
    }

    public update() {
        this.updatePosition();
    }

    public updatePosition() {
        // const render = Container.get(Render);
        // this.setPosition(new Point(render.width / 2, render.height / 2));
    }

    public pointInCircle(fi: number, r: number) {
        const x = r * Math.cos(fi);
        const y = r * Math.sin(fi);
        return [x, y];
    }
}
