import {Entity} from '../abstracts/Entity';
import {Render} from '../Render';
import {Container} from 'typedi';

export class Map extends Entity {

    public size = 25;
    public zoom = 10;
    private render: Render;

    constructor() {
        super();
    }

    public createMap(map: any) {

        let grid: any = this.container.getChildByName('grid');
        if (grid) {
            this.container.removeChild(grid);
        }

        grid      = new PIXI.Graphics();
        grid.name = 'grid';
        grid.lineStyle(1, 0xdddddd, .8);

        grid.moveTo(0, 0);

        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {
                if (map[i][j]) {
                    grid.lineStyle(2, 0xF44336, 1);
                    grid.drawCircle(i * this.zoom + this.zoom / 2, j * this.zoom + this.zoom / 2, this.zoom / 2 - 2);
                    grid.lineStyle(1, 0xdddddd, .8);
                }
                grid.drawRect(i * this.zoom, j * this.zoom, this.zoom - 2, this.zoom - 2);
            }
        }
        grid.endFill();
        this.container.addChild(grid);
        this.container.position.x = 20;
        this.container.position.y = 100;
        this.render               = Container.get(Render);
        this.container.zIndex     = 0;
        this.render.ui.addChild(this.container);
    }
}