import {Entity} from '../abstracts/Entity';
import {Point} from '../helpers/cyber/Point';
import * as filters from 'pixi-filters';
import {Container} from 'typedi';
import {Network} from '../Network';

export class PlayerTitle extends Entity {
    public padding = new Point(20, 10);

    public text: PIXI.Text;

    private box: PIXI.Graphics;

    constructor(item: { id: string, name: string }) {
        super();
        this.drawName(item.name);
        this.id = item.id;
    }

    public drowBox(width: number) {
        this.box = new PIXI.Graphics();
        this.box.lineStyle(2, 0x340E00, 1);
        this.box.beginFill(0x0C672D, 0.8);
        this.box.drawRoundedRect(0, 0, this.padding.x + width, this.padding.y + this.text.height, 5);
        this.box.endFill();

        this.container.addChild(this.box);
        this.container.addChild(this.text);

        this.container.interactive = true;
        this.container.buttonMode  = true;
        this.container.on('pointerdown', () => {
            const net = Container.get(Network);
            net.changeOwner(this.id);
        });

        this.placementText();
    }

    public drawName(name: string) {
        const style    = new PIXI.TextStyle({
            fontFamily     : 'Arial',
            fontSize       : 18,
            fontStyle      : 'italic',
            fontWeight     : 'bold',
            stroke         : '#1a6f1d',
            strokeThickness: 5,
        });
        this.text      = new PIXI.Text(name, style);
        this.text.name = 'playerName';
        this.text.anchor.set(0.5);
    }

    public placementText() {
        this.text.position = new PIXI.Point(this.box.width / 2, this.box.height / 2);
    }

    public select() {
        const outlineFilterBlue = new filters.OutlineFilter(2, 0x99ff99);
        this.box.filters        = [outlineFilterBlue];
    }

    public unselect() {
        this.box.filters = [];
    }

    public addOutline() {
        const outline          = new filters.OutlineFilter(2, 0xFF8800);
        this.container.filters = [outline];
    }

    public removeOutline() {
        this.container.filters = [];
    }
}
